const { resolve } = require('path');

module.exports = {
  output: {
    filename: 'bundle.js',
    publicPath: '/',
    path: resolve(__dirname, '../public')
  },
  context: resolve(__dirname, '../src'),
  resolve: {
    extensions: [ '.js', '.json', '.scss', '.css', '.svg', '.pdf', '.zip' ],
    alias: {
      stylesheets: resolve(__dirname, '../src/scss'),
      javascripts: resolve(__dirname, '../src/js')
    }
  }
};
