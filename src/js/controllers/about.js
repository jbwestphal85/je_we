import { fadeInElement, insertDataUser } from '../utils/helpers';
import User from '../components/user';

class About {
  constructor() {}

  initialize() {
    new User().initialize();

    // condition to render different kinds of editForm
    // depends on user screen resolution
    window.innerWidth <= 640
      ? this.handleFormEditMobile()
      : this.handleFormEditDesktop();
  }

  // Edit form for desktop screens
  handleFormEditDesktop() {
    const $btnEdit = document.querySelectorAll('.form-profile-item-btn');
    const $btnCancel = document.querySelectorAll('.form-profile-item-cancel');
    const $form = document.querySelector('#formUser');

    Array.from($btnEdit).forEach(btn => {
      btn.addEventListener('click', event => {
        event.preventDefault();
        this.openFormPopup(btn);
      });
    });

    Array.from($btnCancel).forEach(btn => {
      btn.addEventListener('click', event => {
        event.preventDefault();
        this.closeFormPopup();
      });
    });

    $form.addEventListener('submit', event => {
      event.preventDefault();
      this.closeFormPopup();
      this.handleFormSubmit($form);
    });
  }

  // Edit form for mobile screens
  handleFormEditMobile() {
    const $btnEdit = document.querySelector('.btn-form-edit-mobile');
    const $btnCancel = document.querySelector('#btnCancelEditMobile');
    const $formActions = document.querySelector('.form-profile-actions');
    const $formItems = document.querySelectorAll('.form-profile-item-text');
    const $formFields = document.querySelectorAll('.form-profile-edit');
    const $form = document.querySelector('#formUser');

    $btnEdit.addEventListener('click', event => {
      event.preventDefault();
      [].forEach.call($formItems, function(el) {
        el.style.display = 'none';
      });
      [].forEach.call($formFields, function(el) {
        fadeInElement(el);
      });

      $btnEdit.style.display = 'none';
      fadeInElement($formActions);
    });

    $btnCancel.addEventListener('click', event => {
      this.closeFormEdit(
        event,
        $formItems,
        $formFields,
        $formActions,
        $btnEdit
      );
    });

    $form.addEventListener('submit', event => {
      event.preventDefault();
      this.closeFormEdit(
        event,
        $formItems,
        $formFields,
        $formActions,
        $btnEdit
      );
      this.handleFormSubmit($form);
    });
  }

  /**
   * Opens the popup fields (triggers in desktop)
   * @param {Object} btn A child button to close the popup
   */
  openFormPopup(btn) {
    btn.classList.add('active');
    btn.nextElementSibling.classList.add('active');
  }

  // Closes the popup fields (triggers in desktop)
  closeFormPopup() {
    const $popup = document.querySelectorAll('.form-profile-edit');
    const $btnEdit = document.querySelectorAll('.form-profile-item-btn');

    [].forEach.call($popup, function(el) {
      el.classList.remove('active');
    });

    [].forEach.call($btnEdit, function(el) {
      el.classList.remove('active');
    });
  }

  /**
   * Events to show/hide elements when clicked Cancel (triggers in mobiles)
   * @param {Object} event An event object
   * @param {Object[]} formItems An array of fields to update data
   * @param {Object[]} formFields An array of inputs from form
   * @param {Object} formActions An array of items to
   * @param {Object} btn An array of items to
   */
  closeFormEdit(event, formItems, formFields, formActions, btn) {
    event.preventDefault();
    [].forEach.call(formItems, function(el) {
      fadeInElement(el);
    });
    [].forEach.call(formFields, function(el) {
      el.style.display = 'none';
    });

    formActions.style.display = 'none';
    fadeInElement(btn);
  }

  /**
   * Handle form submit, triggers in desktop and mobile.
   * @param {Object} form The edit form
   */
  handleFormSubmit(form) {
    // get user data from local storage
    let userDataStorage = JSON.parse(localStorage.getItem('userData'));

    // input values from form
    const nameValue = document.querySelector('#nameUser').value;
    const firstNameValue = document.querySelector('#firstNameUser').value;
    const lastNameValue = document.querySelector('#lastNameUser').value;
    const phoneValue = document.querySelector('#phoneUser').value;
    const addressValue = document.querySelector('#addressUser').value;
    const websiteValue = document.querySelector('#websiteUser').value;

    userDataStorage.profile.address = addressValue;
    userDataStorage.profile.phone = phoneValue;
    userDataStorage.profile.website = websiteValue;

    insertDataUser('.address-user', addressValue);
    insertDataUser('.phone-user', phoneValue);
    insertDataUser('.website-user', websiteValue);

    if (window.innerWidth <= 640) {
      userDataStorage.profile.name = firstNameValue + ' ' + lastNameValue;
      insertDataUser('.name-user', firstNameValue + ' ' + lastNameValue);
    } else {
      userDataStorage.profile.name = nameValue;
      insertDataUser('.name-user', nameValue);
    }

    localStorage.setItem('userData', JSON.stringify(userDataStorage));
  }
}

export default About;
