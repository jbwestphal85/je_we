import { getUrlImg } from '../utils/helpers';
import User from './user';

class Header {
  constructor() {}

  initialize() {
    this.handleCoverUpdate();

    // initialize user data only for header fields
    new User().handleUserData();
  }

  // update user cover, changes header background
  handleCoverUpdate() {
    const $header = document.querySelector('.header');
    const $uploadField = document.querySelector('#uploadCoverField');

    $uploadField.addEventListener('change', function() {
      getUrlImg(this, $header);
    });
  }
}

export default Header;
