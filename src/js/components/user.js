import {
  fetchData,
  updateInputField,
  insertDataUser,
  maskPhone
} from '../utils/helpers';

class User {
  constructor() {}

  initialize() {
    this.handleUserData();
    this.handleInputsUpdate();
  }

  /**
   * Get all element fields to render and fetch user data from a json file
   * Sets the received data in local storage to persist future submits
   */
  handleUserData() {
    let userData = {};
    const $userPicture = document.querySelector('.picture-user');
    let userDataStorage = localStorage.getItem('userData');

    if (userDataStorage == null) {
      // fetch data from user in some json
      fetchData('./data/userProfile.json').then(data => {
        userData = data.profile;

        userDataStorage = localStorage.setItem(
          'userData',
          JSON.stringify(data)
        );

        insertDataUser('.name-user', userData.name);
        insertDataUser('.address-user', userData.address);
        insertDataUser('.phone-user', userData.phone);
        insertDataUser('.website-user', userData.website);
        insertDataUser('.reviews-user', userData.followers);
        insertDataUser('.followers-user', userData.reviews);

        $userPicture.src = userData.picture;

        if (document.getElementById('formUser')) {
          this.handleNameUser();
        }
      });
    } else {
      userData = JSON.parse(userDataStorage);

      insertDataUser('.name-user', userData.profile.name);
      insertDataUser('.address-user', userData.profile.address);
      insertDataUser('.phone-user', userData.profile.phone);
      insertDataUser('.website-user', userData.profile.website);
      insertDataUser('.reviews-user', userData.profile.followers);
      insertDataUser('.followers-user', userData.profile.reviews);

      $userPicture.src = userData.profile.picture;

      if (document.getElementById('formUser')) {
        this.handleNameUser();
      }
    }

    maskPhone('.phone-user');
  }

  /**
   * Change classes of parent's field if input has value
   */
  handleInputsUpdate() {
    const $inputFields = document.querySelectorAll('.form-profile-edit-input');

    // loop through all input fields
    // check if has value to update class
    // call updateInputField on event listeners to update class if has value
    Array.from($inputFields).forEach(input => {
      if (input.value) {
        input.parentNode.classList.add('form-profile-edit-field--focused');
      }

      input.addEventListener('keydown', event => {
        updateInputField(input);
      });

      input.addEventListener('change', event => {
        updateInputField(input);
      });
    });
  }

  /**
   * Method to split user name in firstName and lastName
   */
  handleNameUser() {
    const $nameUser = document.querySelector('#nameUser');
    const $firstNameUser = document.querySelector('#firstNameUser');
    const $lastNameUser = document.querySelector('#lastNameUser');

    const splitName = $nameUser.value.split(' ');
    const lastname = splitName.reduce(
      (acc, current, index) => (index == 1 ? current : `${acc} ${current}`)
    );

    $firstNameUser.value = splitName[0];
    $lastNameUser.value = lastname;
    this.handleInputsUpdate();
  }
}

export default User;
