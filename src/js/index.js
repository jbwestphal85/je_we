import { fetchContent } from './utils/helpers';
import Header from './components/header';
import About from './controllers/about';
import Settings from './controllers/settings';

class MainApp {
  constructor() {
    this.$header = document.getElementById('header');
    this.$view = document.getElementById('view');
  }

  initialize() {
    const hash = window.location.hash.replace('#', '');

    // Load header component
    this.handleHeaderLoad();

    // Loads markup content depending of the browser's hash
    !window.location.hash
      ? this.handleViewMarkup('about', this.$view)
      : this.handleViewMarkup(hash, this.$view);
  }

  /**
   * Loads Header html component and class.
   * Loads methos for navigaton.
   */
  handleHeaderLoad() {
    fetchContent('./components/header.html').then(html => {
      this.$header.innerHTML = html;
      new Header().initialize();
      this.handleNavigation();
    });
  }

  /**
   * Handle navigation of application.
   */
  handleNavigation() {
    const $view = document.getElementById('view');
    const $navItems = document.querySelectorAll('.nav-item');

    Array.from($navItems).forEach(link => {
      link.addEventListener('click', event => {
        event.preventDefault();

        const path = link.getAttribute('href').replace('#', '');
        const slug = path.replace('#', '');

        this.handleViewMarkup(slug, $view);
      });
    });
  }

  /**
   * Fetch view depending of browser hash.
   * Loads view controller and activate menu link.
   * @param {string} slug Slug from a view.
   * @param {Object} view HTML component to load some view.
   */
  handleViewMarkup(slug, view) {
    fetchContent(`./views/${slug}.html`).then(html => {
      view.innerHTML = html;
      window.location.hash = slug;
      this.handleViewController(slug);
      this.handleViewActiveNav(slug);
    });
  }

  /**
   * Gets controller from some given view.
   * @param {string} slug Slug from a view.
   */
  handleViewController(slug) {
    if (slug === 'about') {
      new About().initialize();
    } else if (slug === 'settings') {
      new Settings().initialize();
    }
  }

  /**
   * Include active class in nav depending on given item.
   * @param {Object} item Nav item object to compare
   */
  handleViewActiveNav(item) {
    const $navItems = document.querySelectorAll('.nav-item');

    [].forEach.call($navItems, function(el) {
      el.classList.remove('active');

      if (el.href.indexOf(item) > -1) {
        el.classList.add('active');
      }
    });
  }
}

new MainApp().initialize();
