import 'promise-polyfill/src/polyfill';
import 'whatwg-fetch';

/**
 * Shows a 404 page if the request throws error
 * @param {string} response The requested url
 * @return {Promise} Promise from fetch request
 */
export function handleFetchError(response) {
  const $navItems = document.querySelectorAll('.nav-item');

  if (!response.ok) {
    return fetchContent(`./views/404.html`).then(html => {
      document.getElementById('view').innerHTML = html;
      window.location.hash = '';
      [].forEach.call($navItems, function(el) {
        el.classList.remove('active');
      });
    });
  }
  return response;
}

/**
 * Fetch HTML content from a url/file
 * @param {string} url The requested url
 * @return {Promise} Promise from fetched request
 */
export const fetchContent = url =>
  fetch(url)
    .then(handleFetchError)
    .then(response => response.text());

/**
 * Fetch data from a url/file and returns json
 * @param {string} url The requested url
 * @return {Promise} Promise from fetched request
 */
export const fetchData = url =>
  fetch(url)
    .then(res => res.json())
    .then(data => data);

/**
 * Get Image url from input file and assign it as background for a HTML element.
 * @param {Object} input Input file, generally "this"
 * @param {Object} element HTML element
 */
export function getUrlImg(input, element) {
  if (input.files && input.files[0]) {
    const reader = new FileReader();

    reader.onload = function(e) {
      element.style.backgroundImage = `url(${e.target.result})`;
    };

    reader.readAsDataURL(input.files[0]);
  }
}

/**
 * Adds class to input parent if input has value
 * @param {Object} element The object input to update
 */
export function updateInputField(element) {
  const input = element;

  setTimeout(function() {
    const val = input.value;
    if (val != '')
      input.parentNode.classList.add('form-profile-edit-field--focused');
    else input.parentNode.classList.remove('form-profile-edit-field--focused');
  }, 1);
}

/**
 * Shows an element with fadeIn animation
 * @param {Object} el The object to show
 */
export function fadeInElement(el) {
  el.style.opacity = 0;
  el.style.display = 'block';

  let last = +new Date();
  let showProgressEl = function() {
    el.style.opacity = +el.style.opacity + (new Date() - last) / 400;
    last = +new Date();

    if (+el.style.opacity < 1) {
      (window.requestAnimationFrame && requestAnimationFrame(showProgressEl)) ||
        setTimeout(showProgressEl, 16);
    }
  };

  showProgressEl();
}

/**
 * Gets all user fields and update value in elements html or inputs
 * @param {string} classEl A class to loop through
 * @param {Object} data Data to be shown
 */
export const insertDataUser = (classEl, data) => {
  const items = document.querySelectorAll(classEl);

  [].forEach.call(items, function(el) {
    if (el.value === '') {
      el.value = data;
      updateInputField(el);
    } else {
      el.innerHTML = data;
    }
  });
};

/**
 * Input mask for US phones
 * @param {Object} input The input to receive mask
 */
export const maskPhone = input => {
  const inputEl = document.querySelectorAll(input);

  Array.from(inputEl).forEach(input => {
    input.addEventListener('input', e => {
      const x = e.target.value
        .replace(/\D/g, '')
        .match(/(\d{0,3})(\d{0,3})(\d{0,5})/);
      e.target.value = !x[2]
        ? x[1]
        : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');
    });
  });
};
