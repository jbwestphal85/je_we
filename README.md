# JE_WE

Project stack:

* Sass/Postcss
* Lint (Eslint, Prettier e Stylelint)
* Precommit Lints
* Visual Studio Code config like Chrome Debugger and Lint Integration

# What you need?

* Node.js 8.9.4 or more
* NPM or Yarn

# Install

After clone the repo execute:

```sh
$ npm install or yarn install
```

# Setup of your IDE

To maintain code quality and always have a standard across all of the team's
project has rules defined for javascript and css / scss. We use the Eslint /
Prettier for Javascript and Stylelint for SCSS. It is necessary to
integration of these rules with your favorite IDE. We recommend using Visual
Studio Code with the following plugins:

| Plugin    | README                                                                                     |
| --------- | ------------------------------------------------------------------------------------------ |
| ESlint    | [check plugin](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) |
| Prettier  | [check plugin](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode) |
| stylelint | [check plugin](https://marketplace.visualstudio.com/items?itemName=shinnn.stylelint)       |

# Pre-commit

Before running git commit, the rules of ESLint and Stylelint are executed. If
there is an error or some non-default code of the site will generate an error
and you will not be able to commit.

# Running the project

Here are the commands that are used in the project:

| Command            | Description                                    |
| ------------------ | ---------------------------------------------- |
| gulp               | Run the project in development with livereload |
| npm/yarn run build | Generates a js build for production            |

The project runs in http://localhost/je_we/public using the Chrome extension LiveReload
