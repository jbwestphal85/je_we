const { resolve } = require('path');

const config = {
  entry: [
    'babel-polyfill',
    'whatwg-fetch',
    resolve(__dirname, './src/js/index.js')
  ],
  devtool: 'cheap-source-map',
  output: {
    filename: 'bundle.js',
    publicPath: '/',
    path: resolve(__dirname, './public/assets/js')
  },
  context: resolve(__dirname, './src'),
  mode: 'development',
  watch: true,
  resolve: {
    extensions: [ '.js', '.json', '.scss', '.css', '.svg', '.pdf', '.zip' ],
    alias: {
      stylesheets: resolve(__dirname, '../src/scss'),
      javascripts: resolve(__dirname, '../src/js')
    }
  },
  module: {
    rules: [
      {
        test: /\.css?$/,
        use: [ 'css-loader' ]
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [ 'file-loader' ]
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [ 'file-loader' ]
      }
    ]
  }
};

module.exports = config;
