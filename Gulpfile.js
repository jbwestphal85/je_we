// Essentials
const gulp = require('gulp');
const webpack = require('webpack');
const webpackStream = require('webpack-stream');

// Sass Requires
const sass = require('gulp-sass');
const autoprefix = require('gulp-autoprefixer');
const rename = require('gulp-rename');
const sourcemaps = require('gulp-sourcemaps');

// Utilities
const gutil = require('gulp-util');
const chalk = require('chalk');
const notify = require('gulp-notify');
const livereload = require('gulp-livereload');

const config = require('./webpack.config.js');

// Gulp Default Task
gulp.task('default', [ 'watch', 'webpack', 'scss' ]);

// Gulp Webpack
gulp.task('webpack', function() {
  return gulp
    .src('./src/js/index.js')
    .pipe(webpackStream(config, webpack))
    .on('error', function(error) {
      notify().write({
        message: error.message
      });
      this.emit('end');
    })
    .pipe(gulp.dest('./public/assets/js'))
    .pipe(
      notify({
        title: 'Webpack',
        message: 'Generated file: <%= file.relative %>'
      })
    )
    .pipe(livereload());
});

// Gulp SCSS
gulp.task('scss', function() {
  return gulp
    .src('./src/scss/styles.scss')
    .pipe(sourcemaps.init())
    .pipe(
      sass({
        style: 'compressed'
      }).on('error', function(error) {
        gutil.log(chalk.yellow(error.message));
        notify().write({
          message: error.message
        });
        this.emit('end');
      })
    )
    .pipe(autoprefix('last 2 versions'))
    .pipe(rename('styles.css'))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./public/assets/css'))
    .pipe(
      notify({
        title: 'Sass',
        message: 'Generated file: <%= file.relative %>'
      })
    )
    .pipe(livereload());
});

// $ gulp watch
gulp.task('watch', function() {
  livereload.listen();

  gulp.watch([ './src/scss/**/*.scss' ], [ 'scss' ]);

  gulp.watch([ './src/build/styles.css' ], function(file) {
    livereload.changed(file);
  });
});
